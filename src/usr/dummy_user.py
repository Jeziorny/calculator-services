from random import randrange
from time import sleep

import requests

if __name__ == '__main__':
    while True:
        params = {
            'arg1': randrange(10),
            'arg2': randrange(10),
            'operation': 'add' if randrange(2) else 'sub',
        }
        r = requests.get(url='http://127.0.0.1:5000/', params=params)
        print(r.json())
        sleep(5)
