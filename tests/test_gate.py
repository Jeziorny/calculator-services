from unittest.mock import patch, Mock, MagicMock

import pytest

from src.gate.app import create_app

_SERVICE_ENDPOINT_URL = '/?arg1={value_1}&arg2={value_2}&operation={operation}'


@pytest.fixture
def client():
    app = create_app()
    with app.test_client() as client:
        yield client


@patch('src.gate.app.requests.get', return_value='pulpeciki')
@pytest.mark.parametrize('operation, expected_service_url', [
    ('add', 'http://add:5000/'),
    ('sub', 'http://subtract:5000/'),
])
def test_gate_calls_add_service(requests_get_mock, operation, expected_service_url, client):
    url = _SERVICE_ENDPOINT_URL.format(value_1=3, value_2=5, operation=operation)

    response = client.get(url)

    requests_get_mock.assert_called_once_with(
        url=expected_service_url,
        params={'arg1': '3', 'arg2': '5'}
    )
    assert response.data == b'pulpeciki'
    assert response.status_code == 200


